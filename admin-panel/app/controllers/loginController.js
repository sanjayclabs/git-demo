app.controller('LoginController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT) {


    //initially set those objects to null to avoid undefined error
    $scope.login = {};
    $scope.doLogin = function () {

        $.post(MY_CONSTANT.url + '/admin_login',
            {

                user_email: $scope.login.email,
                password: $scope.login.password

            },

            function (data) {

                data = JSON.parse(data);


                if (data.error) {


                    window.location.href = "#/login";
                    alert(data.error);

                } else {

                    var someSessionObj = {'accesstoken': data.access_token};
                    //console.log(someSessionObj);
                    $cookies.dotobject = someSessionObj;
                    $scope.usingCookies = {
                        'cookies.dotobject': $cookies.dotobject,
                        "cookieStore.get": $cookieStore.get('dotobject')
                    };

                    $cookieStore.put('obj', someSessionObj);
                    $scope.usingCookieStore = {
                        "cookieStore.get": $cookieStore.get('obj'),
                        'cookies.dotobject': $cookies.obj
                    };

                    console.log($cookieStore.get('obj').accesstoken);

                    window.location.href = "#/dashboard";


                }


            });


    };
    $scope.forgotFile = function (emailId) {

        console.log(emailId);
        $.post(MY_CONSTANT.url + '/forgot_password',
            {

                email: emailId

            },

            function (data) {

                data = JSON.parse(data);
                console.log(data);


                if (data.flag == '0') {
                    alert(data.error);
                }
                else if (data.flag == '7') {
                    alert(data.error);
                }
                else {
                    alert(data.log);
                    window.location.reload();
                }


            })


    };


    $scope.resetFile = function (pass, pass1) {

        if (pass != pass1) {
            alert("Password and confirm password must be equal");
        }
        var url = document.URL;

        $.post(MY_CONSTANT.url + '/reset_password',
            {

                reset_token: url.substr(url.lastIndexOf('=') + 1),
                new_password: pass
            },

            function (data) {

                data = JSON.parse(data);
                console.log(data);


                if (data.log) {
                    alert(data.log);
                    window.location.href = "#/login";

                }

            })
    };


});

app.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }


});
app.controller('DashboardController', function ($scope, $http, $route, $cookieStore, $timeout, $location, MY_CONSTANT) {


    if (typeof $cookieStore.get('obj') == "undefined") {
        window.location.href = "#/login";
        return;
    }
    $scope.changePassword = function (curpass, newpass, confirmpass) {

        if (newpass != confirmpass) {
            alert("Password and confirm password must be equal");
        }
        else {

            $.post(MY_CONSTANT.url + '/change_password',
                {

                    access_token: $cookieStore.get('obj').accesstoken,
                    old_password: curpass,
                    new_password: newpass


                },


                function (data) {

                    data = JSON.parse(data);
                    console.log(data);


                    if (data.error) {
                        alert(data.error);
                    }
                    else {
                        alert(data.log);
                        window.location.reload();
                    }


                })

        }


    };

    $scope.currentPage = 1; //current page
    $scope.entryLimit = 5;

    $.post(MY_CONSTANT.url + '/customer_list', {

        access_token: $cookieStore.get('obj').accesstoken


    }, function (data) {

        var dataArray = [];
        data = JSON.parse(data);
        data.forEach(function (column) {

            var d = {

                user_id: "",
                user_name: "",
                email: "",
                mobile: "",
                created_at: "",
                is_block: ""

            };

            var date = column.registration_datetime.toString().split("T")[0];
            d.user_id = column.user_id;
            d.user_name = column.first_name + " " + column.last_name;

            d.email = column.email;
            d.mobile = column.mobile;
            d.created_at = date;
            d.is_block = column.is_block;
            dataArray.push(d);

        });

        $scope.$apply(function () {
            $scope.list = dataArray;
            $scope.filteredItems = $scope.list.length; //Initially for no filter
            $scope.totalItems = $scope.list.length;
        });

    });

    $scope.getClass = function (path) {
        if ($location.path().substr(0, path.length) == path) {
            return "active"
        } else {
            return ""
        }
    };

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function () {
        $timeout(function () {
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };

    // for block or unblock
    $scope.reloadRoute = function () {
        $route.reload();
    };

    $scope.changeStatus = function (status, userId) {
        console.log(status);
        console.log(userId);
        $.post(MY_CONSTANT.url + '/block_unblock_user',
            {


                access_token: $cookieStore.get('obj').accesstoken,
                user_id: userId,
                new_block_status: status
            }
            ,

            function (data) {

                console.log(data);

            });
    };

});

