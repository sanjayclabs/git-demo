var func = require('./commonfunction');
var sendResponse = require('./sendResponse');
var md5 = require('MD5');
var async = require('async');
var generatePassword = require('password-generator');

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Register From Email
 * INPUT : firstName, lastName, email, mobile, password, deviceType, deviceToken, appVersion
 * OUTPUT : user information
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.RegisterFromEmail = function(req, res) {

    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var email = req.body.email;
    var mobile = req.body.mobile;
    var password = req.body.password;
    var userType = req.body.user_type;   // 1 for customer, 2 for driver
    var deviceType = req.body.device_type;
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;

    console.log(req.body);

    var manValues = [firstName, email, mobile, password, deviceType, appVersion];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function(callback) {
            checkEmailAvailability(res, email, callback);
        },
        function(callback) {
            func.checkAppVersion(deviceType, appVersion, callback);
        },
        function(updatePopup, critical, callback) {

            func.getImageNameAfterUpload(req.files.profile_pic, "profile", function(imageName) {
                callback(updatePopup, critical, imageName);
            });
        }], function(updatePopup, critical, imageName) {

        var loginTime = new Date();
        var accessToken = func.encrypt(email + loginTime);
        var encryptPassword = md5(password);
        imageName = profilePicBaseUrl + imageName;
        var sql = "INSERT INTO `users`(`access_token`, `first_name`, `last_name`, `email`, `mobile`, `password`, `fb_id`, `registration_type`, `user_type`, `device_type`, `device_token`, `updated_at`,`app_version`,`image`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        connection.query(sql, [accessToken, firstName, lastName, email, mobile, encryptPassword, 0, 1, userType, deviceType, deviceToken, loginTime, appVersion, imageName], function(err, response) {

            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {
                var userID = response.insertId;



                if (userType == 2) {
                    var data = {"message": "Thanks for registration. Please wait till the time admin approves your request."}
                }
                else {
                    var data = {access_token: accessToken, update_popup: updatePopup, critical: critical, first_name: firstName, last_name: lastName, mobile: mobile, image: imageName, user_id: userID, email: email, "credits": 0};
                }

                HomeScreenData(function(services) {
                    data.services = services;
                    data.cards = [];
                    sendResponse.sendSuccessData(data, res);
                });
            }
        });
    }
    );
};


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * EmailLogin
 * INPUT : email, password, deviceType, deviceToken, appVersion
 * OUTPUT : user information
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.EmailLogin = function(req, res) {

    var email = req.body.email;
    var password = req.body.password;
    var userType = req.body.user_type;   // 1 for customer, 2 for driver
    var deviceType = req.body.device_type;
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;

    console.log(req.body);

    var manValues = [email, password, deviceType, appVersion];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function(callback) {

            func.checkAppVersion(deviceType, appVersion, callback);

        },
        function(updatePopup, critical, callback) {

            checkEmailPasswordAndUserType(res, updatePopup, critical, email, password, userType, callback);

        }], function(err, updatePopup, critical, response) {

        var userID = response[0].user_id;
        var accessToken = response[0].access_token;
        var loginTime = new Date();


        updateUserParams(deviceToken, deviceType, loginTime, appVersion, userID);

        var data = {access_token: accessToken, update_popup: updatePopup, critical: critical, first_name: response[0].first_name, last_name: response[0].last_name, mobile: response[0].mobile, image: response[0].image, user_id: response[0].user_id, email: email, "credits": response[0].credits};
        getUserCardsData(userID, function(cards) {
            HomeScreenData(function(services) {
                data.services = services;
                data.cards = cards;
                sendResponse.sendSuccessData(data, res);
            });
        });
    }
    );
};


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * check Email Availability
 * INPUT : email
 * OUTPUT : email available or not
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function getLoginInformation(userID, callback) {


    var sql = "SELECT `user_id`, `access_token`, `first_name`, `last_name`, `email`, `image`, `mobile`, `fb_id`, `registration_type`, `user_type` FROM `users` WHERE `user_id`=? limit 1";
    connection.query(sql, [userID], function(err, response) {


    });

}


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * forgot Password
 * INPUT : email
 * OUTPUT : success,error
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.ForgotPassword = function(req, res) {

    var email = req.body.email;
    var manValues = [email];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);

        }, function(callback) {

            getUserIDFromEmail(res, email, callback);

        }], function(err, userID) {


        var link = generatePassword(20, false);
        var date = new Date();
        link = md5(date + link);

        var sql = "UPDATE `users` SET `link_one_time_password`=?,`forgot_password_set`=? WHERE `user_id`=? LIMIT 1";
        connection.query(sql, [link, 1, userID], function(err, response1) {

            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {
                forgotPasswordRequest(email, link, function(result) {

                    if (result == 0) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var data = {"message": "Check your email to change your password."}
                        sendResponse.sendSuccessData(data, res);
                    }
                });
            }
        });
    }
    );
};


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Fb login
 * INPUT : fbId,fbName,fbUsername,fbAccessToken,deviceToken, deviceName, appVersion
 * OUTPUT : Register user and return ACCESS TOKEN
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.FbLogin = function(req, res) {

    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var email = req.body.email;
    var mobile = req.body.mobile;
    var fbId = req.body.fb_id;
    var fbAccessToken = req.body.fb_access_token
    var deviceType = req.body.device_type;
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;
    console.log(req.body)
    var manValues = [email, fbId, fbAccessToken, deviceType, appVersion];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function(callback) {
            func.checkAppVersion(deviceType, appVersion, callback);
        },
        function(updatePopup, critical, callback) {
            checkFbUser(res, updatePopup, critical, fbId, fbAccessToken, callback);
        }], function(err, updatePopup, critical, results) {

        var loginTime = new Date();

        HomeScreenData(function(services) {

            if (results.length) {

                //login
                var userID = results[0].user_id;
                var accessToken = results[0].access_token;
                if(results[0].is_block == 1){
                    var data =  "Your account is blocked. Contact Admin for further details.";
                    sendResponse.sendError(data, res);
                }
                else if(results[0].is_block == 2){
                    var data =  "Your account is deleted. Contact Admin for further details.";
                    sendResponse.sendError(data, res);
                }
                else {


                    updateUserParams(deviceToken, deviceType, loginTime, appVersion, userID);
                    var fbDataScreen = 0;

                    if ((results[0].first_name == '') || (results[0].last_name == '') || (results[0].mobile == '')) {
                        fbDataScreen = 1;
                    }

                    var data = {
                        "new": 0,
                        "access_token": accessToken,
                        update_popup: updatePopup,
                        critical: critical,
                        first_name: results[0].first_name,
                        last_name: results[0].last_name,
                        mobile: results[0].mobile,
                        image: results[0].image,
                        user_id: userID,
                        email: results[0].email,
                        "credits": results[0].credits
                    };
                    getUserCardsData(userID, function (cards) {
                        data.fb_data_screen = fbDataScreen;
                        data.services = services;
                        data.cards = cards;
                        sendResponse.sendSuccessData(data, res);
                    });
                }
            }
            else {

                //register

                checkEmailForFbRegister(email, function(userResult) {

                    if (userResult.length) {

                        var userID = userResult[0].user_id;
                        var accessToken = userResult[0].access_token;
                        firstName = userResult[0].first_name;
                        lastName = userResult[0].last_name;
                        mobile = userResult[0].mobile;
                        var imageName = userResult[0].image;
                        var credits = userResult[0].credits;

                        if(userResult[0].is_block == 1){
                            var data = "Your account is blocked. Contact Admin for further details.";
                            sendResponse.sendError(data, res);
                        }
                        else if(userResult[0].is_block == 2){
                            var data =  "Your account is deleted. Contact Admin for further details.";
                            sendResponse.sendError(data, res);
                        }
                        else {
                            var sql = "UPDATE `users` SET `fb_id`=? ,`registration_type`=? WHERE `user_id`=? limit 1";
                            connection.query(sql, [fbId, 3, userID], function (err, updateResult) {

                                if (err) {
                                    sendResponse.somethingWentWrongError(res);
                                }
                                else {

                                    updateUserParams(deviceToken, deviceType, loginTime, appVersion, userID);

                                    var fbDataScreen = 0;

                                    var data = {
                                        "new": 0,
                                        "access_token": accessToken,
                                        update_popup: updatePopup,
                                        critical: critical,
                                        first_name: firstName,
                                        last_name: lastName,
                                        mobile: mobile,
                                        image: imageName,
                                        user_id: userID,
                                        email: email,
                                        "credits": credits
                                    };
                                    getUserCardsData(userID, function (cards) {
                                        data.fb_data_screen = fbDataScreen;
                                        data.services = services;
                                        data.cards = cards;
                                        sendResponse.sendSuccessData(data, res);
                                    });
                                }
                            });
                        }
                    }
                    else {

                        var accessToken = func.encrypt(fbId);
                        var imageName = 'https://graph.facebook.com/' + fbId + '/picture?height=200&width=200'

                        var sql = "INSERT INTO `users`(`access_token`, `first_name`, `last_name`, `email`, `mobile`, `fb_id`, `registration_type`, `user_type`, `device_type`, `device_token`, `updated_at`,`app_version`,`image`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
                        connection.query(sql, [accessToken, firstName, lastName, email, mobile, fbId, 2, 1, deviceType, deviceToken, loginTime, appVersion, imageName], function(err, response) {

                            if (err) {
                                sendResponse.somethingWentWrongError(res);
                            }
                            else {
                                var userID = response.insertId;
                                var fbDataScreen = 0;

                                if ((firstName == '') || (lastName == '') || (mobile == '')) {
                                    fbDataScreen = 1;
                                }

                                var data = {"new": 1, "access_token": accessToken, update_popup: updatePopup, critical: critical, first_name: firstName, last_name: lastName, mobile: mobile, image: imageName, user_id: userID, email: email, "credits": 0};
                                data.fb_data_screen = fbDataScreen;
                                data.services = services;
                                data.cards = [];
                                sendResponse.sendSuccessData(data, res);
                            }
                        });
                    }
                });
            }
        });
    }
    );
};


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * user Logout
 * INPUT : email
 * OUTPUT : success,error
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.UserLogout = function(req, res) {

    var accessToken = req.body.access_token;
    var manValues = [accessToken];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);

        }, function(callback) {

            var extraDataNeeded = [];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

        }], function(err, userInfo) {

        var userID = userInfo[0].user_id;

        var sql = "UPDATE `users` SET `device_type`=?,`device_token`=? WHERE `user_id`=? limit 1";
        connection.query(sql, [0, 0, userID], function(err, response) {

            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {
                var data = {"message": "Logged out successfully."}
                sendResponse.sendSuccessData(data, res);
            }
        });
    }
    );
};


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Access Token Login
 * INPUT : email, password, deviceType, deviceToken, appVersion
 * OUTPUT : user information
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.AccessTokenLogin = function(req, res) {

    var accessToken = req.body.access_token;
    var deviceType = req.body.device_type;
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;

    console.log(req.body);

    var manValues = [accessToken, deviceType, appVersion];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function(callback) {

            var extraDataNeeded = ['first_name', 'last_name', 'mobile', 'image', 'email', 'credits','is_block'];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);
        },
        function(response, callback) {

            func.checkAppVersion(deviceType, appVersion, function(err, updatePopup, critical) {
                callback(null, updatePopup, critical, response);
            });

        }], function(err, updatePopup, critical, response) {

        var userID = response[0].user_id;
        var loginTime = new Date();
            if(response[0].is_block == 1){
                var data = "Your account is blocked. Contact Admin for further details.";
                sendResponse.sendError(data, res);
            }
            else if(response[0].is_block == 2){
                var data =  "Your account is deleted. Contact Admin for further details.";
                sendResponse.sendError(data, res);
            }
            else {

                updateUserParams(deviceToken, deviceType, loginTime, appVersion, userID);

                var data = {
                    access_token: accessToken,
                    update_popup: updatePopup,
                    critical: critical,
                    first_name: response[0].first_name,
                    last_name: response[0].last_name,
                    mobile: response[0].mobile,
                    image: response[0].image,
                    user_id: userID,
                    email: response[0].email,
                    "credits": response[0].credits
                };
                getUserCardsData(userID, function (cards) {
                    HomeScreenData(function (services) {
                        data.services = services;
                        data.cards = cards;
                        sendResponse.sendSuccessData(data, res);
                    });
                });
            }
    }
    );
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Give rating to technician
 * INPUT : accessToken, bookingId,rating
 * OUTPUT : rating given to technician
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.InitialCall = function (req, res) {

    var accessToken = req.body.access_token;
    var deviceType = req.body.device_type;
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;

    var manValues = [accessToken];

    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function (callback) {

            var extraDataNeeded = ['first_name', 'last_name', 'mobile', 'image', 'email', 'credits','is_block'];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

        },
        function(userInfo, callback) {

            func.checkAppVersion(deviceType, appVersion, function(err, updatePopup, critical) {
                callback(null, updatePopup, critical, userInfo);
            });

        }], function(err, updatePopup, critical, userInfo) {
        var userId = userInfo[0].user_id;
        var loginTime = new Date();
        if(userInfo[0].is_block == 1){
            var data = "Your account is blocked. Contact Admin for further details.";
            sendResponse.sendError(data, res);
        }
        else if(userInfo[0].is_block == 2){
            var data =  "Your account is deleted. Contact Admin for further details.";
            sendResponse.sendError(data, res);
        }
        else {

            updateUserParams(deviceToken, deviceType, loginTime, appVersion, userId);
            var sql1 = "SELECT a.`id`,a.`technician_id`,b.`name`,a.`booking_id` as master_booking_id,b.`description`,a.`address`,a.`local_start_time`,a.`service_price`,a.`service_type`," +
                "a.`service_time`,b.`image`,a.`status`,a.`schedule_occurrence_type`,a.`latitude`,a.`longitude`, a.`tech_rating`, a.`last_updated` FROM `booking_timings` a, `service` b " +
                "WHERE  a.`service_id`=b.`service_id` && a.`user_id` = ? && a.`status` NOT IN(0,1,6,7) LIMIT 1";
            connection.query(sql1, [userId], function (err, responseState) {

                console.log(err)
                console.log(responseState)

                var responseStateCount = responseState.length;
                if (responseStateCount == 0) {
                    // var data = {"flag": 0,""};            // home screen
                    //sendResponse.sendSuccessData(data, res);


                        var data = {
                            access_token: accessToken,
                            update_popup: updatePopup,
                            critical: critical,
                            first_name: userInfo[0].first_name,
                            last_name: userInfo[0].last_name,
                            mobile: userInfo[0].mobile,
                            image: userInfo[0].image,
                            user_id: userId,
                            email: userInfo[0].email,
                            "credits": userInfo[0].credits,
                            "flag": 0
                        };
                        getUserCardsData(userId, function (cards) {
                            HomeScreenData(function (services) {
                                data.services = services;
                                data.cards = cards;
                                sendResponse.sendSuccessData(data, res);
                            });
                        });

                }
                else {
                    responseState[0].local_start_time = responseState[0].local_start_time.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                    responseState[0].image = servicePicBaseUrl + responseState[0].image;
                    var techId = responseState[0].technician_id;
                    if (responseState[0].status == 2 || responseState[0].status == 3) {   // on the way
                        responseState[0].last_updated = 0;
                        getTechInfoForStateCall(techId, function (responseTechnician) {


                            var data = {"schedule_data": responseState, "tech_info": responseTechnician, "flag": 1};

                            sendResponse.sendSuccessData(data, res);

                        });


                    }
                    else if (responseState[0].status == 4) {      // service started
                        getTechInfoForStateCall(techId, function (responseTechnician) {

                            var dbDate = responseState[0].last_updated.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                            var startDate = new Date(dbDate);
                            var timeDiff = func.getTimeDifference(startDate, 2);
                            console.log(timeDiff)
                            responseState[0].last_updated = timeDiff;
                            var data = {"schedule_data": responseState, "tech_info": responseTechnician, "flag": 2};

                            sendResponse.sendSuccessData(data, res);

                        });

                    }
                    else if (responseState[0].status == 5) {      // service ended
                        if (responseState[0].tech_rating == 0) {      // show rating screen
                            getTechInfoForStateCall(techId, function (responseTechnician) {
                                responseState[0].last_updated = 0;

                                var data = {"schedule_data": responseState, "tech_info": responseTechnician, "flag": 3};

                                sendResponse.sendSuccessData(data, res);

                            });
                        }
                        else {                                       // home screen
                            // var data = {"flag": 0};
                            //sendResponse.sendSuccessData(data, res);

                                var data = {
                                    access_token: accessToken,
                                    update_popup: updatePopup,
                                    critical: critical,
                                    first_name: userInfo[0].first_name,
                                    last_name: userInfo[0].last_name,
                                    mobile: userInfo[0].mobile,
                                    image: userInfo[0].image,
                                    user_id: userId,
                                    email: userInfo[0].email,
                                    "credits": userInfo[0].credits,
                                    "flag": 0
                                };
                                getUserCardsData(userId, function (cards) {
                                    HomeScreenData(function (services) {
                                        data.services = services;
                                        data.cards = cards;
                                        sendResponse.sendSuccessData(data, res);
                                    });
                                });


                        }


                    }
                }
            });
        }
    });
};

function getTechInfoForStateCall(techId, callback) {
    var sql = "SELECT  `technician_id`,`first_name`,`last_name`,`image`,`technician_type`,`mobile`,`total_rating`,`rating_count`, `latitude`, `longitude` FROM `technicians` WHERE `technician_id`=? LIMIT 1";
    connection.query(sql, [techId], function (err, responseTechnician) {


        var rating = 5;
        if (responseTechnician[0].rating_count != 0) {
            rating = parseInt(responseTechnician[0].total_rating / responseTechnician[0].rating_count);
        }
        responseTechnician[0].rating = rating;
        delete responseTechnician[0].total_rating;
        delete responseTechnician[0].rating_count;

        return callback(responseTechnician);
    });
}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * get card of user
 * INPUT : userId
 * OUTPUT : card data fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function getUserCardsData(userId, callback) {

    var sql = "SELECT `card_id`,  `customer_id`, `last_4`, `card_type`, `default_status` FROM `card` WHERE `user_id` = ?";
    connection.query(sql, [userId], function(err, responseCards) {

        callback(responseCards);

    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Authenticate fb user
 * INPUT : fbId,fbAccessToken
 * OUTPUT : Authenticated or not
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function checkFbUser(res, updatePopup, critical, userFbId, fbAccessToken, callback) {

    var request = require('request');
    var urls = "https://graph.facebook.com/" + userFbId + "?fields=updated_time&access_token=" + fbAccessToken;

    request(urls, function(error, response, body) {
        if (!error && response.statusCode == 200) {

            var output = JSON.parse(body);
            if (output['error']) {
                var error = 'Not an authenticated user.';
                sendResponse.sendError(error, res);
            }
            else {
                var sql = "SELECT `user_id`, `access_token`, `first_name`, `last_name`, `email`, `image`, `mobile`, `fb_id`, `credits`,`is_block` FROM `users` WHERE `fb_id`=? LIMIT 1";
                connection.query(sql, [userFbId], function(err, userResponse) {

                    if (err) {
                        console.log(err);
                        sendResponse.somethingWentWrongError(res);

                    }
                    else {

                        return callback(null, updatePopup, critical, userResponse);
                    }
                });
            }
        }
        else {
            var error = 'Not an authenticated user.';
            sendResponse.sendError(error, res);
        }
    });
}


function getUserIDFromEmail(res, email, callback) {

    var sql = "SELECT `user_id` FROM `users` WHERE `email`=? && `is_block`=? LIMIT 1";
    connection.query(sql, [email,0], function(err, responseUser) {

        if (responseUser.length == 0) {
            var error = 'Email not registered';
            sendResponse.sendError(error, res);
        }
        else {

            var userID = responseUser[0].user_id;
            callback(null, userID);
        }
    });
}

function forgotPasswordRequest(email, link, callback) {

    var to = email;
    var sub = "[Vizavoo] Please reset your password";
    //var link1 = "api.vizavoo.com/customer_password/forget_password.php?token="+link;
    var msg = "Hi, <br><br>";
    msg += "We have received a password change request for your account.<br>";
    msg += "If you made this request, then";
    msg += "<a href = "+genVarSettings.domainName+"/customer_password/forget_password.php?token=" + link + "> click this link to reset your password. </a><br>";
    msg += "If you did not ask to change your password, then please ignore this email. Another user may have entered your email by mistake. No changes will be made to your account. <br><br>";
    msg += "Thanks,<br>Vizavoo Team. <br>";

    func.sendEmail(to, msg, sub, function(result) {
        return callback(result);
    });
}


/*
 * ------------------------------------------------------
 *  Check link
 * Input: token
 * Output: token valid or not
 * ------------------------------------------------------
 */
exports.checkTokenFromLink = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Content-Type', 'application/json');
    var token = req.body.token;


    var sql = "SELECT user_id FROM users WHERE link_one_time_password=? LIMIT 1";
    connection.query(sql, [token], function(err, response) {
        if (response.length == 1)
        {

            var response1 = {"log": 'Success.'};
            res.send(JSON.stringify(response1));


        }
        else
        {
            var response1 = {"error": 'Invalid token.'};
            res.send(JSON.stringify(response1));
        }
    });


};

/*
 * ------------------------------------------------------
 *  set password
 * Input: email, password
 * Output: password set
 * ------------------------------------------------------
 */
exports.setPasswordFromUserEmailAndPassword = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Content-Type', 'application/json');
    var token = req.body.token;
    var password = req.body.password;

    var manValues = [token, password];
    var checkData = func.checkBlank(manValues);
    if (checkData == 1)
    {

        var response = {"error": 'Some parameter missing.', "flag": 1};
        res.send(JSON.stringify(response));
    }
    else
    {
        if (password.length < 6) {

            var response1 = {"error": 'Your password must be at least 6 characters long.', "flag": 1};
            res.send(JSON.stringify(response1));
        }
        else {
            password = md5(password);

            var sql = "UPDATE users set password=?,link_one_time_password=?,forgot_password_set = ? WHERE link_one_time_password=? LIMIT 1";
            connection.query(sql, [password, '', 0, token], function(err, responseUpdate) {
                if (err)
                {

                    var response1 = {"error": 'Password not set.'};
                    res.send(JSON.stringify(response1));

                }
                else
                {
                    var response1 = {"log": 'Password set successfully.'};
                    res.send(JSON.stringify(response1));
                }

            });
        }
    }

};

function checkEmailPasswordAndUserType(res, updatePopup, critical, email, password, userType, callback) {


    var encryptPassword = md5(password);

    var sql = "SELECT `user_id`, `access_token`,`first_name`,`last_name`,`mobile`,`image`,`credits`,`is_block` FROM `users` WHERE `email`=? and `password`=? and `user_type`=? LIMIT 1";
    connection.query(sql, [email, encryptPassword, userType], function(err, response) {

        if (!response.length) {
            var error = 'The email or password you entered is incorrect.';
            sendResponse.sendError(error, res);
        }
        else if(response[0].is_block == 1){
            var data = "Your account is blocked. Contact Admin for further details.";
            sendResponse.sendError(data, res);
        }
        else if(response[0].is_block == 2){
            var data =  "Your account is deleted. Contact Admin for further details.";
            sendResponse.sendError(data, res);
        }
        else if (userType == 2) {
            var error = 'Your request is under consideration. Please wait till the time admin approves your request.';
            sendResponse.sendError(error, res);
        }
        else {
            callback(null, updatePopup, critical, response);
        }
    });
}


function checkEmailForFbRegister(email, callback) {

    var sql = "SELECT `user_id`, `access_token`,`first_name`,`last_name`,`mobile`,`image`,`credits`,`is_block` FROM `users` WHERE `email`=?  and `user_type`=?";
    connection.query(sql, [email, 1], function(err, response) {

        return callback(response);
    });
}


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * check Email Availability
 * INPUT : email
 * OUTPUT : email available or not
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function checkEmailAvailability(res, email, callback) {

    var sql = "SELECT `user_id` FROM `users` WHERE `email`=? limit 1";
    connection.query(sql, [email], function(err, response) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else if (response.length) {
            var error = 'Email already exists.';
            sendResponse.sendError(error, res);
        }
        else {
            callback();
        }
    });
}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Update user parameters
 * INPUT : deviceToken, deviceType, loginTime, appVersion, userID
 * OUTPUT : Update the params
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function updateUserParams(deviceToken, deviceType, loginTime, appVersion, userID) {

    var sql = "UPDATE `users` SET `device_type`=?,`device_token`=?,`app_version`=?,`updated_at`=? WHERE `user_id`=? limit 1";
    connection.query(sql, [deviceType, deviceToken, appVersion, loginTime, userID], function(err, result) {
    });
}


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * HomeScreenData
 * OUTPUT : Update the params
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function HomeScreenData(callback) {

    var sql = "SELECT `service_id`,`name`, `description`, `price`, `duration`, `image` FROM `service` WHERE `service_type`=? and `active_status`=?";
    connection.query(sql, [1, 1], function(err, service1) {

        var sql = "SELECT `service_id`,`name`, `description`, `price`, `duration`, `image` FROM `service` WHERE `service_type`=? and `active_status`=?";
        connection.query(sql, [2, 1], function(err, service2) {

            var numberOfHairService = service1.length;
            var numberOfMakeUpService = service2.length;

            for (var i = 0; i < numberOfHairService; i++) {
                (function(i) {
                    service1[i].image = servicePicBaseUrl + service1[i].image;

                })(i)
            }

            for (var j = 0; j < numberOfMakeUpService; j++) {
                (function(j) {

                    service2[j].image = servicePicBaseUrl + service2[j].image;

                })(j)
            }


            var services = {hair_style: service1, make_up: service2};

            return callback(services);
        });
    });
}


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * View booking details
 * INPUT : accessToken, bookingId
 * OUTPUT : booking's details fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.SendAndroidPush = function(req, res) {
    var androidDevices = ['APA91bGFyyLNrZ79tCC9g77eQQkPSuCMsj4F0BH0RihG4UtN4O4BXCPi_NetH_rpw9sOPFyJgzFXi2SVma0kJz3lQmvpcRXomNAlZQ4DUVWlRk8JIp0Iffxh5cI3RmvyFue2HfRj9nbn5jyIbRGlpmYuyiQogSNBCQ'];
    var message = 'Testing Testing';
    func.sendAndroidPushNotification(androidDevices, message);
    var data = {"message": "Done"};
    sendResponse.sendSuccessData(data, res);
};