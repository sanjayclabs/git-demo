AWSSettings = require('../config/ApplicationSettings').getAWSSettings();
genVarSettings = require('../config/ApplicationSettings').generalVariableSettings();
emailSettings = require('../config/ApplicationSettings').emailSettings();
var sendResponse = require('./sendResponse');

/*
 * ------------------------------------------------------
 * Check if manadatory fields are not filled
 * INPUT : array of field names which need to be mandatory
 * OUTPUT : Error if mandatory fields not filled
 * ------------------------------------------------------
 */

exports.checkBlank = function (arr) {

    return (checkBlank(arr));
};

function checkBlank(arr) {
    var arrlength = arr.length;
    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == '') {
            return 1;
            break;
        }
        else if (arr[i] == undefined) {
            return 1;
            break;
        }
        else if (arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}

exports.checkBlankWithCallback = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}

/*
 * ------------------------------------------------------
 *  Authenticate a user
 *  INPUT : user_access_token
 *  OUTPUT : user_id,brand_id,email,timezone,currency
 * ------------------------------------------------------
 */
exports.authenticateUser = function (accessToken, callback) {

    var sql = "SELECT `user_id`, `first_name`, `last_name`, `gender`, `image`, `email`, `fb_id`, `access_token`, `device_type`, `device_token`, `app_version`, `created_at`, `updated_at`, `delete_status` FROM `users` WHERE `access_token`=? LIMIT 1";
    connection.query(sql, [accessToken], function (err, result) {

        return callback(result);
    });
};


/*
 * ------------------------------------------------------
 * Authenticate a user through Access token and return extra data
 * Input:Access token{Optional Extra data}
 * Output: User_id Or Json error{Optional Extra data}
 * ------------------------------------------------------
 */
exports.authenticateAccessTokenAndReturnExtraData = function (accesstoken, arr, res, callback) {

    var sql = "SELECT `user_id`";
    arr.forEach(function (entry) {
        sql += "," + entry;
    });
    sql += " FROM `users`";
    sql += " WHERE `access_token`=? LIMIT 1";
    connection.query(sql, [accesstoken], function (err, result) {

        if (err) {
            sendResponse.parameterMissingError(res);
        }
        else if (result.length > 0) {

            return callback(null, result);

        } else {
            sendResponse.invalidAccessTokenError(res);

        }
    });

}


/*
 * ------------------------------------------------------
 *  Get Number Of Cards For The User
 *  INPUT : userID
 *  OUTPUT : number of cards
 * ------------------------------------------------------
 */

exports.GetNumberOfCardsForTheUser = function (userID, callback) {

    var sql = "SELECT `card_id` FROM `card` WHERE `user_id`=?";
    connection.query(sql, [userID], function (err, response) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else {
            callback(null,userID,response.length);
        }
    });
};


/*
 * ------------------------------------------------------
 *  check App Version
 *  INPUT : appVersion
 *  OUTPUT : update popup and critical
 * ------------------------------------------------------
 */

exports.checkAppVersion = function (deviceType, appVersion, callback) {

    var sql = "SELECT `id`, `type`, `version`, `critical`,`last_critical` FROM `app_version` WHERE `type`=? && `app_type`=? limit 1";
    connection.query(sql, [deviceType,0], function (err, response) {

        appVersion = parseInt(appVersion);

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else if (response[0].version > appVersion) {
            var critical = response[0].critical;
            if(response[0].last_critical > appVersion)
                {
                    critical = 1;
                }
            callback(null, 1, critical);
        }
        else {
            callback(null, 0, 0);
        }
    });
};


/*
 * ------------------------------------------------------
 *  Authenticate a user
 *  INPUT : user_access_token
 *  OUTPUT : user_id,brand_id,email,timezone,currency
 * ------------------------------------------------------
 */
// targeting_order used in battle.js
exports.authenticateAdmin = function (userAccessToken, callback) {

    var sql = "SELECT `admin_id`,access_token FROM `tb_admin` WHERE `access_token`=? LIMIT 1";
    connection.query(sql, [userAccessToken], function (err, result) {

        if (result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};

/*
 * -----------------------------------------------------------------------------
 * Uploading image to s3 bucket
 * INPUT : file parameter
 * OUTPUT : image path
 * -----------------------------------------------------------------------------
 */
function uploadImageToS3Bucket(file, folder, callback) {

    var fs = require('fs');
//    var mathjs = require('mathjs');
//    var math = mathjs();
    var AWS = require('aws-sdk');

    if (file == undefined) {
        return callback("logo.jpeg");
    }
    else {
        var filename = file.name; // actual filename of file
        var path = file.path; //will be put into a temp directory
        var mimeType = file.type;

        fs.readFile(path, function (error, file_buffer) {

            if (error) {
                //  console.log(error)
                return callback("logo.jpeg");
            }
            else {
                var length = 5;
                var str = '';
                var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                var size = chars.length;
                for (var i = 0; i < length; i++) {

                    var randomnumber = Math.floor(Math.random() * size);
                    str = chars[randomnumber] + str;
                }

                filename = "vizavoo-" + str + "-" + file.name;
                filename = filename.split(' ').join('-');

                AWS.config.update({accessKeyId: AWSSettings.awsAccessKey, secretAccessKey: AWSSettings.awsSecretKey});
                var s3bucket = new AWS.S3();
                var params = {Bucket: AWSSettings.awsBucket, Key: folder + '/' + filename, Body: file_buffer, ACL: 'public-read', ContentType: mimeType};

                s3bucket.putObject(params, function (err, data) {
                    if (err) {
                        //  console.log(err)
                        return callback("logo.jpeg");
                    }
                    else {
                        return callback(filename);
                    }
                });
            }
        });
    }
};
/*
 * -----------------------------------------------------------------------------
 * sorting an array in ascending order
 * INPUT : array and key according to which sorting is to be done
 * OUTPUT : sorted array
 * -----------------------------------------------------------------------------
 */
exports.sortByKeyAsc = function (array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
};
/*
 * -----------------------------------------------------------------------------
 * sorting an array in descending order
 * INPUT : array and key according to which sorting is to be done
 * OUTPUT : sorted array
 * -----------------------------------------------------------------------------
 */
exports.sortByKeyDesc = function (array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
};

/*
 * -----------------------------------------------------------------------------
 * return time difference of cur date and another time
 * INPUT : time
 * OUTPUT : diff of 2 times
 * -----------------------------------------------------------------------------
 */
exports.getTimeDifference = function(time, flag)
{
    var today = new Date();

    if (flag == 1)
    {
        var diffMs = (time - today); // milliseconds between post date & now
    }
    else
    {
        var diffMs = (today - time); // milliseconds between now & post date
    }

    var seconds = Math.floor(0.001 * diffMs);  // in seconds

    return seconds;

};

///*
// * -----------------------------------------------------------------------------
// * return time difference of cur date and another time
// * INPUT : time
// * OUTPUT : diff of 2 times
// * -----------------------------------------------------------------------------
// */
//exports.getTimeDifference = function (time) {
//
//    //var Math = require('mathjs');
//    var mathjs = require('mathjs');
//    var Math = mathjs();
//
//    var today = new Date();
//    //var diffMs = (today - time); // milliseconds between now & post date
//    var diffMs = (time - today); // milliseconds between now & post date
//
//    var diffDays = Math.floor(diffMs / 86400000); // days
//    var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
//    var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
//    var postTime = {"days": diffDays, "hours": diffHrs, "minutes": diffMins};
//
//    return postTime;
//};

/*
 * -----------------------------------------------------------------------------
 * Encryption code
 * INPUT : string
 * OUTPUT : crypted string
 * -----------------------------------------------------------------------------
 */
exports.encrypt = function (text) {

    var crypto = require('crypto');
    var cipher = crypto.createCipher('aes-256-cbc', 'd6F3Efeq');
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}


exports.sendEmail = function (receiverMailId, message, subject, callback) {
    var nodemailer = require("nodemailer");
    var smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: emailSettings.username,
            pass: emailSettings.password
        }
    });

// setup e-mail data with unicode symbols
    var mailOptions = {
        from: "VizaVoo <"+emailSettings.username+">", // sender address
        to: receiverMailId, // list of receivers
        subject: subject, // Subject line
        html: message // plaintext body
    }

// send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            return callback(0);
        } else {
            return callback(1);
        }

        // if you don't want to use this transport object anymore, uncomment following line
        //smtpTransport.close(); // shut down the connection pool, no more messages
    });
};

exports.sendFeedbackEmail = function (senderEmail, message, subject, callback) {

    var nodemailer = require("nodemailer");
    var smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: emailSettings.username,
            pass: emailSettings.password
        }
    });

// setup e-mail data with unicode symbols
    var mailOptions = {
        from: senderEmail, // sender address
        to: emailSettings.feedback_mail, // list of receivers
        subject: subject, // Subject line
        replyTo: senderEmail,
        text: message // plaintext body
    }

// send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (err, response) {
        if (err) {
            return callback(err);
        } else {
            return callback(null,1);
        }

        // if you don't want to use this transport object anymore, uncomment following line
        //smtpTransport.close(); // shut down the connection pool, no more messages
    });
};


exports.generateRandomString = function () {
    var mathjs = require('mathjs');
    math = mathjs();
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 6; i++)
        text += possible.charAt(math.floor(math.random() * possible.length));

    return text;
};

exports.getDateTime = function (date,flag) {

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day = date.getDate();
    day = (day < 10 ? "0" : "") + day;
if(flag == 0){
    return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
}
else{
    return year + "-" + month + "-" + day + " " + hour + ":" + min;
}

};

/*
 * -----------------------------------------------------------------------------
 * return time difference of cur date and another time
 * INPUT : time
 * OUTPUT : diff of 2 times
 * -----------------------------------------------------------------------------
 */
exports.getDaysDifference = function(time, flag)
{
    //var Math = require('mathjs');
    var today = new Date();
    if (flag == 1)
    {
        var diffMs = (time - today); // milliseconds between now & post date
    }
    else
    {
        var diffMs = (today - time); // milliseconds between now & post date
    }
    var diffDays = Math.floor(diffMs / 86400000); // days
//    var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
//    var diffMins = Math.floor(((diffMs % 86400000) % 3600000) / 60000); // minutes
//    var diffSecs = Math.floor((((diffMs % 86400000) % 3600000) % 60000)/1000); // seconds
//    var postTime = {"days": diffDays, "hours": diffHrs, "minutes": diffMins,"seconds":diffSecs};

    return diffDays;

};


exports.getImageNameAfterUpload = function (file, folder, callback) {

    if ((file) && (file.name)) {
        uploadImageToS3Bucket(file, folder, function (fileName) {
            return callback(fileName);
        });
    }
    else {
        return callback("logo.jpeg")
    }
};



/*
 * -----------------------------------------------------------------------------
 * Sending push notification to devices
 * INPUT : iosDeviceToken,message
 * OUTPUT : Notification send
 * -----------------------------------------------------------------------------
 */
exports.sendApplePushNotification = function(iosDeviceToken, message)
{
    var apns = require('apn');
    var deviceCount = iosDeviceToken.length;
    if(deviceCount != 0)
        {
            
    var options = {
        cert: genVarSettings.pemFile,
        certData: null,
        key: genVarSettings.pemFile,
        keyData: null,
        passphrase: 'click',
        ca: null,
        pfx: null,
        pfxData: null,
        gateway: genVarSettings.iosGateway,
        port: 2195,
        rejectUnauthorized: true,
        enhanced: true,
        cacheLength: 100,
        autoAdjustCache: true,
        connectionTimeout: 0,
        ssl: true
    }
    
    for(var i=0; i<deviceCount; i++)
        {
             var deviceToken1 = iosDeviceToken[i].replace(/[^0-9a-f]/gi, "");

    if((deviceToken1.length) % 2)
    {
        console.log("error")
    }
    else
    {
    var deviceToken = new apns.Device(iosDeviceToken[i]);
    var apnsConnection = new apns.Connection(options);
    var note = new apns.Notification();

    note.expiry = Math.floor(Date.now() / 1000) + 3600;
    note.badge = 0;
    note.sound = 'ping.aiff';
    note.alert = message;
    //note.payload  = {"event_id": eventId, "flag": flag};
    apnsConnection.pushNotification(note, deviceToken);

// i handle these events to confirm the notification gets
// transmitted to the APN server or find error if any

    function log(type) {
        return function() {
            console.log(type, arguments);
        }
    }

    apnsConnection.on('transmitted', function() {


    });

    apnsConnection.on('error', log('error'));
    apnsConnection.on('transmitted', log('transmitted'));
    apnsConnection.on('timeout', log('timeout'));
    apnsConnection.on('connected', log('connected'));
    apnsConnection.on('disconnected', log('disconnected'));
    apnsConnection.on('socketError', log('socketError'));
    apnsConnection.on('transmissionError', log('transmissionError'));
    apnsConnection.on('cacheTooSmall', log('cacheTooSmall'));
    }
        }
        }
};


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * sendIosPushNotification
 * INPUT : deviceToken, message
 *
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */


exports.sendAndroidPushNotification = function(resulTokens,text)
{
    var gcm = require('node-gcm');
    var message = new gcm.Message({
        
        collapseKey: 'demo',
        delayWhileIdle: false,
        timeToLive: 2419200,
         data: {
            message: text
        }
    });
    var sender = new gcm.Sender(genVarSettings.androidKey);
     var registrationIds = resulTokens;
    // registrationIds.push(resulTokens)
     console.log(message)
    sender.sendNoRetry(message, registrationIds, function(err, result) {
                                            console.log(err);
                                            console.log(result);
    });
}
