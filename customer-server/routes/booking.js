var func = require('./commonfunction');
var sendResponse = require('./sendResponse');
var md5 = require('MD5');
var async = require('async');
var generatePassword = require('password-generator');

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Party Booking
 * INPUT : accessToken,bookingTitle,numberOfGuests,bookingDate,address,latitude,longitude
 * OUTPUT : error,success
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.PartyBooking = function (req, res) {

    var accessToken = req.body.access_token;
    var bookingTitle = req.body.booking_title;
    var numberOfGuests = req.body.number_of_guests;
    var bookingDate = req.body.booking_date;
    var offset = parseInt(req.body.offset);

    console.log(req.body);
    var manValues = [accessToken, bookingTitle, numberOfGuests, bookingDate, offset];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            }], function (err, userInfo) {

            var userID = userInfo[0].user_id;
            var localBookingDate = new Date(bookingDate);
            localBookingDate.setMinutes(localBookingDate.getMinutes() + offset);
            var sql = "INSERT INTO `party_booking`(`user_id`, `booking_title`, `number_of_guests`, `appointment_date`,`local_appointment_date`,`offset`) VALUES (?,?,?,?,?,?)";
            connection.query(sql, [userID, bookingTitle, numberOfGuests, bookingDate, localBookingDate, offset], function (err, response) {

                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    var data = {"message": "Party booked successfully."}
                    sendResponse.sendSuccessData(data, res);
                }
            });
        }
    );
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Normal Booking(one time, daily, weekly, monthly)
 * INPUT : accessToken,bookingTitle,numberOfGuests,bookingDate,address,latitude,longitude
 * OUTPUT : error,success
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.NormalBooking = function (req, res) {


    var accessToken = req.body.access_token;
    var serviceId = req.body.service_id;
    var cardId = req.body.card_id;
    var bookingDate = req.body.booking_date;
    var address = req.body.address;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var scheduleType = req.body.schedule_type;   // 1-one time, 2-daily, 3-weekly, 4-monthly
    var limit = req.body.limit;
    var offset = parseInt(req.body.offset);
    var servicePrice = req.body.service_price;
    var serviceType = req.body.service_type;   // 1 for hair, 2 for makeup
    var serviceTime = req.body.service_time;
    var city = req.body.city;

    console.log(req.body);
    var manValues = [accessToken, scheduleType, limit, bookingDate, address, latitude, longitude, serviceId, cardId, offset, servicePrice, serviceType, serviceTime];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            }], function (err, userInfo) {

            var userID = userInfo[0].user_id;
            var sql = "SELECT `technician_id` FROM `technicians` WHERE `city`=? && `technician_type`=? && `driver_verified` =?";
            connection.query(sql, [city, serviceType, 1], function (err, responseZipTechnicians) {
                var responseZipTechniciansCount = responseZipTechnicians.length;
                if (responseZipTechniciansCount == 0) {

                    var error = 'Coming soon to your area please choose other location.';
                    sendResponse.sendError(error, res);
                    //var data = {"error": "Booking not done."}
                    //   sendResponse.sendSuccessData(data, res);
                }
                else {
                    var sql = "INSERT INTO `booking`( `user_id`, `service_id`, `latitude`, `longitude`, `address`, `card_id`, `schedule_occurrence_type`,`occurrence_no`,`service_price`,`service_type`,`service_time`, `city`)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
                    connection.query(sql, [userID, serviceId, latitude, longitude, address, cardId, scheduleType, limit, servicePrice, serviceType, serviceTime, city], function (err, response) {

                        if (err) {
                            console.log(err)
                            sendResponse.somethingWentWrongError(res);
                        }
                        else {
                            var bookingId = response.insertId;

                            var booking = [];
                            var bookingTime = [];
                            var bookingLocalTime = [];
                            var durationInMinutes = 0;
                            var currentDate = new Date();
                            for (var j = 0; j < limit; j++) {

                                var bookingDate1 = new Date(bookingDate);
                                var bookingDateLocal = new Date(bookingDate);

                                if (scheduleType == 1) {
                                    bookingDate1.setMinutes(bookingDate1.getMinutes() + durationInMinutes);
                                    bookingDateLocal.setMinutes(bookingDateLocal.getMinutes() + ((durationInMinutes - 0) + (offset - 0)));
                                }
                                else if (scheduleType == 2) {
                                    bookingDate1.setMinutes(bookingDate1.getMinutes() + durationInMinutes);
                                    bookingDateLocal.setMinutes(bookingDateLocal.getMinutes() + ((durationInMinutes - 0) + (offset - 0)));
                                    durationInMinutes = durationInMinutes + 1440;
                                }
                                else if (scheduleType == 3) {
                                    bookingDate1.setMinutes(bookingDate1.getMinutes() + durationInMinutes);
                                    bookingDateLocal.setMinutes(bookingDateLocal.getMinutes() + ((durationInMinutes - 0) + (offset - 0)));
                                    durationInMinutes = durationInMinutes + 10080;
                                }
                                else if (scheduleType == 4) {
                                    bookingDate1.setMinutes(bookingDate1.getMinutes() + durationInMinutes);
                                    bookingDateLocal.setMinutes(bookingDateLocal.getMinutes() + ((durationInMinutes - 0) + (offset - 0)));
                                    durationInMinutes = durationInMinutes + 43200;
                                }

                                console.log('dff' + durationInMinutes)
                                booking.push(bookingId, bookingDate1, bookingDateLocal, offset, userID, serviceId, cardId, servicePrice, latitude, longitude, address, serviceType, serviceTime, scheduleType, city, currentDate);
                                bookingTime.push(bookingDate1);
                                bookingLocalTime.push(bookingDateLocal);
                            }


                            var string1 = "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?),";
                            var insert = string1.repeat(limit - 1);
                            insert = insert + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                            // meals= meals.split(",")
                            var sql = "INSERT INTO `booking_timings`(`booking_id`, `start_time`,`local_start_time`,`offset`,`user_id`,`service_id`,`card_id`,`service_price`, `latitude`, `longitude`, `address`,`service_type`,`service_time`,`schedule_occurrence_type`,`city`,`created_at`) VALUES " + insert + "";
                            connection.query(sql, booking, function (err, resultBooking) {
                                if (err) {
                                    console.log(err)
                                    sendResponse.somethingWentWrongError(res);
                                }
                                else {
                                    var data = {"message": "Booking made successfully."}
                                    sendResponse.sendSuccessData(data, res);
                                    var serviceTechs = [];
                                    for(var w=0; w<responseZipTechniciansCount; w++){
                                        serviceTechs.push(responseZipTechnicians[w].technician_id);
                                    }
                                    var serviceTechsStr = serviceTechs.toString();
                                    assignTechnician(resultBooking.insertId, bookingTime, bookingLocalTime, serviceTime, limit, userID, serviceTechsStr);
                                }

                            });


                        }
                    });
                }
            });
        }
    );
};

function assignTechnician(bookingId, bookingTime, bookingLocalTime, serviceTime, limit, userID, serviceTechsStr) {
    console.log('asd')
//    var sql = "SELECT `technician_id` FROM `technicians` WHERE `zipcode`=? && `technician_type`=? && `driver_verified` =?";
//    connection.query(sql, [zipcode, serviceType, 1], function(err, responseZipTechnicians) {
//
//        var responseZipTechniciansCount = responseZipTechnicians.length;
    var bookingIds = [];
    for (var m = 0; m < limit; m++) {

        bookingIds.push(bookingId);
        bookingId = bookingId + 1;
    }
    //    if (responseZipTechniciansCount > 0) {
    console.log('zxc')
    for (var a = 0; a < limit; a++) {
        (function (a) {
            var daysDiff = func.getDaysDifference(bookingTime[a], 1);
            console.log('daysDiff' + daysDiff)
            if (daysDiff < 14) {


                findTechsToSendRequest(bookingIds[a], bookingTime[a], bookingLocalTime[a], serviceTime, userID, serviceTechsStr, function (technicianGot) {
                    console.log('technicianGot' + technicianGot)
                    if (technicianGot == 0) {
                        var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id`=? LIMIT 1";
                        connection.query(sql, [6, bookingIds[a]], function (err, responseUpdate) {
                            //bookingId = bookingId + 1;
                        });
                    }
//                            else {
//                                bookingId = bookingId + 1;
//                            }

                });
            }
//                    else {
//                        console.log('else here')
//                        var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id`=? LIMIT 1";
//                        connection.query(sql, [6, bookingIds[a]], function(err, responseUpdate) {
//                            bookingId = bookingId + 1;
//                        });
//                    }
        })(a);
    }

//        }
//        else {
//            
//            console.log('him')
//            var bookingIdsStr = bookingIds.toString();
//            var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id` IN (" + bookingIdsStr + ")";
//            connection.query(sql, [6], function(err, responseUpdate) {
//
//            });
//        }
//
//    });

//}
}

function findTechsToSendRequest(bookingId, bookingTime, bookingLocalTime, serviceTime, userID, serviceTechsStr, callback) {
    console.log('bookingId' + bookingId)
    //var serviceTechsStr = serviceTechs.toString();
    var buffer = 30;
    var startTime = new Date(bookingLocalTime);
    var endTime = new Date(bookingLocalTime);
    startTime.setMinutes(startTime.getMinutes() - buffer);
    endTime.setMinutes(endTime.getMinutes() + ((buffer - 0) + (serviceTime - 0)));

    console.log('startTime' + startTime)
    console.log('endTime' + endTime)
    //startTime = func.getDateTime(startTime,0);
    //endTime = func.getDateTime(endTime,0);

    //startTime = startTime.split(" ");
    //endTime = endTime.split(" ");

    //var bookingdate = startTime[0];
    var time = startTime;
    var initialTime = startTime;
    initialTime = func.getDateTime(initialTime, 1);
    initialTime = initialTime.split(" ");
    var StartDate = initialTime[0];
    initialTime = initialTime[1];

    console.log('initialTime' + initialTime)

    var timeArr = [initialTime];

    while (new Date(time).getTime() < new Date(endTime).getTime()) {

        var timeInterval = new Date(time);
        timeInterval.setMinutes(timeInterval.getMinutes() + 30);
        timeInterval = func.getDateTime(timeInterval, 1);
        timeInterval = timeInterval.split(" ");
        timeInterval = timeInterval[1];
        timeArr.push(timeInterval);

        time.setMinutes(time.getMinutes() + 30);

    }

    var timeArrCount = timeArr.length;
    var whereCondition = '';
    var passingParams = [];
    for (var i = 0; i < timeArrCount; i++) {
        whereCondition += '`' + timeArr[i] + '` = ? AND';
        passingParams.push(1);
    }
    whereCondition += '`schedule_date` = ? AND `technician_id` IN ('+serviceTechsStr+')';
    passingParams.push(StartDate);

    console.log('whereCondition' + whereCondition)
    console.log('passingParams' + passingParams)

    var sql = "SELECT `technician_id` FROM `availability` WHERE " + whereCondition;
    connection.query(sql, passingParams, function (err, responseAvailableTechnicians) {
        console.log('responseAvailableTechnicians');
        console.log(responseAvailableTechnicians);
        console.log(err)
        var responseAvailableTechniciansCount = responseAvailableTechnicians.length;
        if (responseAvailableTechniciansCount == 0) {
            return callback(0);
        }
        else {
            var availableTechs = [];
            for (var z = 0; z < responseAvailableTechniciansCount; z++) {
                availableTechs.push(responseAvailableTechnicians[z].technician_id);
            }
            var availableTechsStr = availableTechs.toString();
            var sql = "SELECT `technician_id` FROM `booking_timings` WHERE (`local_start_time` BETWEEN ? AND ?) AND (`technician_id` IN (" + availableTechsStr + "))"
            connection.query(sql, [startTime, endTime], function (err, resultFinalTechs) {
                console.log('resultFinalTechs' + resultFinalTechs)
                var resultFinalTechsCount = resultFinalTechs.length;
                var finalTechs = [];
                if (resultFinalTechsCount == 0) {
                    finalTechs = availableTechs;
                }
                else {
                    for (var b = 0; b < resultFinalTechsCount; b++) {
                        finalTechs.push(resultFinalTechs[b].technician_id);
                    }
                    finalTechs = availableTechs.diff(finalTechs);
                }
                var finalTechsCount = finalTechs.length;
                var bookingRequestArray = [];
                for (var c = 0; c < finalTechsCount; c++) {
                    bookingRequestArray.push(bookingId, finalTechs[c], bookingTime, userID)
                }
                var finalTechsStr = finalTechs.toString(',');
                var string1 = "(?,?,?,?),";
                var insert = string1.repeat(finalTechsCount - 1);
                insert = insert + "(?,?,?,?)";

                var message = 'New request arrived.';
                sendPushNotification(finalTechsStr, message);

                // meals= meals.split(",")
                var sql = "INSERT INTO `booking_requests`(`booking_id`, `technician_id`,`booking_datetime`, `user_id`) VALUES " + insert + "";
                connection.query(sql, bookingRequestArray, function (err, resultBookingRequest) {
                    return callback(1);
                });


            });


        }
    });
}


function sendPushNotification(finalTechsStr, message) {
    var sql = "SELECT `device_type`, `device_token` FROM `technicians` WHERE `technician_id` IN (" + finalTechsStr + ")"
    connection.query(sql, function (err, resultTechsDevices) {
        var resultTechsDevicesCount = resultTechsDevices.length;
        var androidDevices = [];
        var iosDevices = [];
        for (var t = 0; t < resultTechsDevicesCount; t++) {
            if (resultTechsDevices[t].device_type == 1) {
                androidDevices.push(resultTechsDevices[t].device_token);
            }
            else {
                iosDevices.push(resultTechsDevices[t].device_token);
            }
        }
        func.sendApplePushNotification(iosDevices, message);
        func.sendAndroidPushNotification(androidDevices, message);

    });
}

Array.prototype.diff = function (a) {
    return this.filter(function (i) {
        return a.indexOf(i) < 0;
    });
};

/*
 * -----------------------------------------------------------------------------
 * prototype for repeating a string
 * INPUT : string and no. of times the string has to be repeated
 * OUTPUT : repeated string
 * -----------------------------------------------------------------------------
 */
String.prototype.repeat = function (num) {
    return new Array(num + 1).join(this);
}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * View my booking(current, past & upcoming)
 * INPUT : accessToken
 * OUTPUT : bookings fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.MyBookings = function (req, res) {

    var accessToken = req.body.access_token;

    var manValues = [accessToken];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            }], function (err, userInfo) {

            var userID = userInfo[0].user_id;

            async.parallel([
                    function (callback) {
                        getOngoingSchedule(res, userID, function (err, ongoing) {

                            callback(err, ongoing);

                        });
                    },
                    function (callback) {
                        getPastSchedules(res, userID, function (err, past) {
                            callback(err, past);
                        });
                    },
                    function (callback) {
                        getUpcomingSchedules(res, userID, function (err, upcoming) {
                            callback(err, upcoming);
                        });
                    }

                ],
// optional callback
                function (err, results) {

                    var data = {"ongoing": results[0], "past": results[1], "upcoming": results[2]}
                    sendResponse.sendSuccessData(data, res);
                });

        }
    );
};


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get ongoing schedule data
 * INPUT : user_id
 * OUTPUT : booking data fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function getOngoingSchedule(res, userID, callback) {
    //var curDate = new Date();
    var sql = "SELECT  a.`id`,a.`booking_id` as master_booking_id,b.`name`,a.`service_type`,a.`status` FROM `booking_timings` a, `service` b WHERE a.`user_id`=? && (a.`status` BETWEEN ? AND ?) && a.`service_id`=b.`service_id` LIMIT 1";
    connection.query(sql, [userID, 2, 4], function (err, responseOngoing) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else {
            if (responseOngoing.length > 0) {
                responseOngoing[0].status = 2;
            }
            callback(null, responseOngoing);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get past schedule data
 * INPUT : user_id
 * OUTPUT : booking data fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function getPastSchedules(res, userID, callback) {
    //var curDate = new Date();
    var sql = "SELECT  a.`technician_id`,a.`id`,a.`booking_id` as master_booking_id,b.`name`,a.`address`,b.`description`,a.`local_start_time`,a.`service_price`,a.`service_time`,a.`service_type`,b.`image`,a.`status`,a.`schedule_occurrence_type` FROM `booking_timings` a, `service` b WHERE a.`user_id`=? && (a.`status`=? || a.`status`=?) && a.`service_id`= b.`service_id`";
    connection.query(sql, [userID, 5, 7], function (err, responsePast) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else {
            var responsePastCount = responsePast.length;

            if (responsePastCount > 0) {
                var technicianArr = [];
                for (var k = 0; k < responsePastCount; k++) {
                    technicianArr.push(responsePast[k].technician_id);
                }
                var technicianArrStr = technicianArr.toString(",");
                var sql = "SELECT  `technician_id`,`first_name`,`last_name`,`image`,`technician_type`,`mobile`,`total_rating`,`rating_count` FROM `technicians` WHERE `technician_id` IN (" + technicianArrStr + ")";
                connection.query(sql, function (err, responseTechnician) {

                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var responseTechnicianCount = responseTechnician.length;
                        for (var i = 0; i < responsePastCount; i++) {

                            responsePast[i].local_start_time = responsePast[i].local_start_time.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                            responsePast[i].image = servicePicBaseUrl + responsePast[i].image;
                            if (responsePast[i].status == 5) {
                                responsePast[i].status = 3;
                            }
                            else {
                                responsePast[i].status = 4;
                            }
                            var techInfo = [];
                            for (var j = 0; j < responseTechnicianCount; j++) {
                                if (responseTechnician[j].technician_id == responsePast[i].technician_id) {
                                    var rating = 5;
                                    if (responseTechnician[j].rating_count != 0) {
                                        rating = parseInt(responseTechnician[j].total_rating / responseTechnician[j].rating_count);
                                    }
                                    responseTechnician[j].rating = rating;
//                                    delete responseTechnician[j].total_rating;
//                                    delete responseTechnician[j].rating_count;

                                    techInfo.push(responseTechnician[j]);
                                    break;
                                }

                            }
                            responsePast[i].tech_info = techInfo;

                        }
                        callback(null, responsePast);
                    }
                });
            }
            else {
                callback(null, responsePast);
            }

        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get upcoming schedule data
 * INPUT : user_id
 * OUTPUT : booking data fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function getUpcomingSchedules(res, userID, callback) {
    var curDate = new Date();
    var sql = "SELECT  a.`id`,a.`booking_id` as master_booking_id,b.`name`,a.`address`,a.`local_start_time`,a.`service_price`,a.`service_type`,b.`image`,a.`status`,a.`schedule_occurrence_type` FROM `booking_timings` a, `service` b WHERE a.`user_id`=? && a.`start_time`>? && a.`service_id`=b.`service_id` && (a.`status` <= ? || a.`status`=?) GROUP BY a.`booking_id`";
    connection.query(sql, [userID, curDate, 1, 6], function (err, responseUpcoming) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else {
            var responseUpcomingCount = responseUpcoming.length;
            for (var i = 0; i < responseUpcomingCount; i++) {
                responseUpcoming[i].local_start_time = responseUpcoming[i].local_start_time.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                responseUpcoming[i].image = servicePicBaseUrl + responseUpcoming[i].image;
                if (responseUpcoming[i].status == 6) {
                    responseUpcoming[i].status = 0;
                }
            }

            callback(null, responseUpcoming);

        }
    });
}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get cancelled schedule data
 * INPUT : user_id
 * OUTPUT : booking data fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function getCancelledSchedules(res, userID, callback) {
    //var curDate = new Date();
    var sql = "SELECT  a.`id`,a.`booking_id` as master_booking_id,b.`name`,a.`address`,a.`local_start_time`,a.`service_price`,a.`service_type`,b.`image`,a.`status`,a.`schedule_occurrence_type` FROM `booking_timings` a, `service` b WHERE a.`user_id`=? && a.`status`=? && a.`service_id`=b.`service_id`";
    connection.query(sql, [userID, 7], function (err, responseCancelled) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else {
            var responsePastCount = responseCancelled.length;
            for (var i = 0; i < responsePastCount; i++) {
                responseCancelled[i].local_start_time = responseCancelled[i].local_start_time.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                responseCancelled[i].image = servicePicBaseUrl + responseCancelled[i].image;
                responseCancelled[i].status = 4;
            }

            callback(null, responseCancelled);
        }
    });
}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * View booking details
 * INPUT : accessToken, bookingId
 * OUTPUT : booking's details fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.BookingDetails = function (req, res) {

    var accessToken = req.body.access_token;
    var bookingId = req.body.booking_id;

    var manValues = [accessToken, bookingId];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            }], function (err, userInfo) {

            var userID = userInfo[0].user_id;

            async.parallel([
                    function (callback) {
                        getScheduleData(res, bookingId, function (err, scheduleInfo) {

                            callback(err, scheduleInfo);

                        });
                    },
                    function (callback) {
                        getTechData(res, bookingId, function (err, techInfo) {
                            callback(err, techInfo);
                        });
                    }

                ],
                function (err, results) {

                    var data = {"schedule_data": results[0], "tech_info": results[1]}
                    sendResponse.sendSuccessData(data, res);
                });

        }
    );
};
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get a specific schedule's data
 * INPUT : user_id,bookingId
 * OUTPUT : booking data fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function getScheduleData(res, bookingId, callback) {
    //var curDate = new Date();
    var sql = "SELECT  a.`id`,b.`name`,a.`booking_id` as master_booking_id,b.`description`,a.`address`,a.`local_start_time`,a.`service_price`,a.`service_type`,a.`service_time`,b.`image`,a.`status`,a.`schedule_occurrence_type`,a.`latitude`,a.`longitude`, a.`last_updated` FROM `booking_timings` a, `service` b WHERE a.`id`=? && a.`service_id`=b.`service_id` LIMIT 1";
    connection.query(sql, [bookingId], function (err, responseSchedule) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else {
            if (responseSchedule.length > 0) {
                var dbDate = responseSchedule[0].last_updated.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                var startDate = new Date(dbDate);
                var timeDiff = func.getTimeDifference(startDate, 2);
                console.log(timeDiff)
                responseSchedule[0].last_updated = timeDiff;

                responseSchedule[0].local_start_time = responseSchedule[0].local_start_time.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                responseSchedule[0].image = servicePicBaseUrl + responseSchedule[0].image;
                if (responseSchedule[0].status == 6) {
                    responseSchedule[0].status = 0;
                }
                else if (responseSchedule[0].status == 7) {
                    responseSchedule[0].status = 4;
                }
                else if (responseSchedule[0].status == 5) {
                    responseSchedule[0].status = 3;
                }
                else if (responseSchedule[0].status >= 2 && responseSchedule[0].status <= 4) {
                    responseSchedule[0].status = 2;
                }

                callback(null, responseSchedule);
            }
            else {
                var error = 'Booking doesn\'t exist.';
                sendResponse.sendError(error, res);
            }
        }
    });
}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get a specific schedule's data
 * INPUT : user_id,bookingId
 * OUTPUT : booking data fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function getTechData(res, bookingId, callback) {
    //var curDate = new Date();
    var sql = "SELECT  a.`technician_id`,b.`first_name`,b.`last_name`,b.`image`,b.`technician_type`,b.`mobile`,b.`total_rating`,b.`rating_count` FROM `booking_timings` a, `technicians` b WHERE a.`id`=? && a.`technician_id`=b.`technician_id` LIMIT 1";
    connection.query(sql, [bookingId], function (err, responseTechnician) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else {
            if (responseTechnician.length > 0) {
                var rating = 5;
                if (responseTechnician[0].rating_count != 0) {
                    rating = parseInt(responseTechnician[0].total_rating / responseTechnician[0].rating_count);
                }
                responseTechnician[0].rating = rating;
                delete responseTechnician[0].total_rating;
                delete responseTechnician[0].rating_count;
            }
            callback(null, responseTechnician);
        }
    });
}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Cancel booking by customer
 * INPUT : accessToken, bookingId
 * OUTPUT : booking's details fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.CancelBooking = function (req, res) {

    var accessToken = req.body.access_token;
    var bookingId = req.body.booking_id;
    var masterBookingId = req.body.master_booking_id;
    var all = req.body.all;   // 0 for one, 1 for all

    var manValues = [accessToken, all];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            }], function (err, userInfo) {

            cancelAllOrOneBooking(bookingId, all, masterBookingId, res, function (responseCancel) {
                if (responseCancel == 1) {
                    var data = {"message": "Booking cancelled successfully"}
                    sendResponse.sendSuccessData(data, res);
                }
                else if (responseCancel == 2) {
                    var data = {"message": "Bookings cancelled successfully"}
                    sendResponse.sendSuccessData(data, res);
                }
                else {
                    sendResponse.somethingWentWrongError(res);
                }
            });


        }
    );
};

function cancelAllOrOneBooking(bookingId, all, masterBookingId, res, callback) {
    var stripe = require("stripe")(genVarSettings.stripeToken);
    if (all == 0) {
        var sql = "SELECT  `status`,`transaction_id`,`technician_id` FROM `booking_timings` WHERE `id`=? LIMIT 1";
        connection.query(sql, [bookingId], function (err, responseBookingStatus) {

            if (responseBookingStatus[0].status >= 2) { // can't cancel now
                var error = 'Booking can\'t be cancelled now.';
                sendResponse.sendError(error, res);
            }
            else if (responseBookingStatus[0].status == 1) { // cancel with refunding
                var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id`=? LIMIT 1";
                connection.query(sql, [7, bookingId], function (err, responseBookingCancel) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {

                        stripe.charges.createRefund(
                            responseBookingStatus[0].transaction_id,
                            {},
                            function (err, refund) {
                                // asynchronously called
                                if (err) {
                                    var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id`=? LIMIT 1";
                                    connection.query(sql, [responseBookingStatus[0].status, bookingId], function (err, responseBookingUnDoCancel) {

                                    });
                                    sendResponse.somethingWentWrongError(res);
                                }
                                else {
                                    var message = 'Booking cancelled by user';
                                    sendPushNotification(responseBookingStatus[0].technician_id, message);
                                    return callback(1);
                                }
                            }
                        );
                    }

                });
            }
            else {   // cancel with no refunding

                var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id`=? LIMIT 1";
                connection.query(sql, [7, bookingId], function (err, responseBookingCancel) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {

                        return callback(1);
                    }
                });
            }

        });
    }
    else {
        var sql = "SELECT `id`,`status`,`transaction_id`,`technician_id` FROM `booking_timings` WHERE `booking_id`=? && `status` < ?"
        connection.query(sql, [masterBookingId, 2], function (err, responseBookingStatus) {

            var responseBookingStatusCount = responseBookingStatus.length;
            if (responseBookingStatusCount == 0) {
                var error = 'Booking can\'t be cancelled now.';
                sendResponse.sendError(error, res);
            }
            else {

                var bookings = [];
                var refundBooking = [];
                var refundTransaction = [];

                for (var j = 0; j < responseBookingStatusCount; j++) {
                    bookings.push(responseBookingStatus[j].id);
                    if (responseBookingStatus[j].status == 1) {
                        refundBooking.push(responseBookingStatus[j].id);
                        refundTransaction.push(responseBookingStatus[j].transaction_id);

                    }
                }

                var bookingsStr = bookings.toString(",");
                var refundBookingCount = refundBooking.length;
                var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id` IN (" + bookingsStr + ")";
                connection.query(sql, [7], function (err, responseBookingCancel) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        if (refundBookingCount > 0) {

                            sendQueries();

                            function sendQueries() {
                                var i = 0;

                                function next() {
                                    if (i < refundBookingCount) {

                                        stripe.charges.createRefund(
                                            refundTransaction[i],
                                            {},
                                            function (err, refund) {
                                                // asynchronously called
                                                if (err) {
                                                    var refundBookingStr = refundBooking.toString(",");
                                                    var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id` IN (" + refundBookingStr + ")";
                                                    connection.query(sql, [1], function (err, responseBookingUnDoCancel) {
                                                        sendResponse.somethingWentWrongError(res);
                                                    });

                                                }
                                                else {
                                                    var message = 'Booking cancelled by user';
                                                    sendPushNotification(responseBookingStatus[i].technician_id, message);
                                                    i++;
                                                    if (i == refundBookingCount) {
                                                        return callback(2);
                                                    }
                                                    else {
                                                        refundBooking.splice(i, 1);
                                                        //delete refundBooking[i];
                                                        next();
                                                    }

                                                }
                                            }
                                        );
                                    }
                                }

                                next();
                            }
                        }
                        else {
                            return callback(2);
                        }
                    }
                });
            }
        });
    }

}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get booking status(technician location, started, ended)
 * INPUT : accessToken, bookingId
 * OUTPUT : booking's status fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.GetBookingStatus = function (req, res) {

    var accessToken = req.body.access_token;
    var bookingId = req.body.booking_id;

    var manValues = [accessToken, bookingId];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            }], function (err, userInfo) {

            var sql = "SELECT a.`status`, b.`latitude`, b.`longitude` FROM `booking_timings` a, `technicians` b WHERE a.`id`=? && a.`technician_id` = b.`technician_id` LIMIT 1"
            connection.query(sql, [bookingId], function (err, responseBookingStatus) {

                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    if (responseBookingStatus[0].status == 2 || responseBookingStatus[0].status == 3) {   // on the way
                        var data = {
                            "flag": 1,
                            "latitude": responseBookingStatus[0].latitude,
                            "longitude": responseBookingStatus[0].longitude
                        }
                        sendResponse.sendSuccessData(data, res);
                    }
                    else if (responseBookingStatus[0].status == 4) {      // service started
                        var data = {"flag": 2}
                        sendResponse.sendSuccessData(data, res);
                    }
                    else if (responseBookingStatus[0].status == 5) {      // service ended
                        var data = {"flag": 3}
                        sendResponse.sendSuccessData(data, res);
                    }

                }

            });

        }
    );
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Give rating to technician
 * INPUT : accessToken, bookingId,rating
 * OUTPUT : rating given to technician
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.GiveRating = function (req, res) {

    var accessToken = req.body.access_token;
    var bookingId = req.body.booking_id;
    var rating = req.body.rating;

    var manValues = [accessToken, bookingId, rating];

    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function (callback) {

            var extraDataNeeded = [];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

        }], function (err, userInfo) {

        var sql = "SELECT `technician_id` FROM `booking_timings` WHERE `id`=? LIMIT 1"
        connection.query(sql, [bookingId], function (err, responseTechnican) {

            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {
                var sql = "UPDATE `booking_timings` SET `tech_rating`=? WHERE `id` = ? LIMIT 1";
                connection.query(sql, [rating, bookingId], function (err, responseBookingRating) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var sql = "UPDATE `technicians` SET `rating_count`=`rating_count`+?, `total_rating`=`total_rating`+? WHERE `technician_id` = ? LIMIT 1";
                        connection.query(sql, [1, rating, responseTechnican[0].technician_id], function (err, responseTechnicianRating) {
                            if (err) {
                                sendResponse.somethingWentWrongError(res);
                            }
                            else {
                                var data = {"message": "Rating done successfully."};
                                sendResponse.sendSuccessData(data, res);
                            }

                        });
                    }
                });

            }

        });
    });
};


exports.ScheduleBookingsCron = function (req, res) {
    var startDate = new Date();
    var startMinutes = 10080;
    startDate.setMinutes(startDate.getMinutes() + startMinutes);
    startDate = func.getDateTime(startDate, 0);

    var endDate = new Date();
    var endMinutes = 10140;
    endDate.setMinutes(endDate.getMinutes() + endMinutes);
    endDate = func.getDateTime(endDate, 0);

    var sql = "SELECT `id`, `user_id`, `service_type` , `service_time`, `city`, `start_time`, `local_start_time`" +
        "  FROM `booking_timings` WHERE `status` = ? && start_time BETWEEN '" + startDate + "' AND '" + endDate + "'";
    connection.query(sql, [0], function (err, responseBookings) {
        var responseBookingsCount = responseBookings.length;
        if (responseBookingsCount == 0) {
            var data = {"message": "No bookings."};
            sendResponse.sendSuccessData(data, res);
        }
        else {
            for (var a = 0; a < responseBookingsCount; a++) {
                (function (a) {
                    //var daysDiff = func.getDaysDifference(bookingTime[a], 1);
                    //console.log('daysDiff' + daysDiff)
                    //if (daysDiff < 14) {
                    var sql = "SELECT `technician_id` FROM `technicians` WHERE `city`=? && `technician_type`=? && `driver_verified` =?";
                    connection.query(sql, [responseBookings[a].city, responseBookings[a].service_type, 1], function (err, responseZipTechnicians) {
                        if (responseZipTechnicians.length != 0) {

                            var serviceTechs = [];
                            for(var w=0; w<responseZipTechniciansCount; w++){
                                serviceTechs.push(responseZipTechnicians[w].technician_id);
                            }
                            var serviceTechsStr = serviceTechs.toString();
                            findTechsToSendRequest(responseBookings[a].id, responseBookings[a].start_time, responseBookings[a].local_start_time, responseBookings[a].service_time, responseBookings[a].user_id,serviceTechsStr, function (technicianGot) {
                                console.log('technicianGot' + technicianGot);
                                if (technicianGot == 0) {
                                    var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id`=? LIMIT 1";
                                    connection.query(sql, [6, responseBookings[a].id], function (err, responseUpdate) {
                                        //bookingId = bookingId + 1;
                                    });
                                }
//                            else {
//                                bookingId = bookingId + 1;
//                            }

                            });
                            // }
//                    else {
//                        console.log('else here')
//                        var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id`=? LIMIT 1";
//                        connection.query(sql, [6, bookingIds[a]], function(err, responseUpdate) {
//                            bookingId = bookingId + 1;
//                        });
//                    }
                        }
                    });

                })(a);
            }
            var data = {"message": "Done"};
            sendResponse.sendSuccessData(data, res);
        }
        // assignTechnician(resultBooking.insertId, bookingTime, bookingLocalTime, serviceTime, limit, userID);
        //var sql = "SELECT `technician_id` FROM `technicians` WHERE `city`=? && `technician_type`=? && `driver_verified` =?";
        //connection.query(sql, [city, serviceType, 1], function(err, responseZipTechnicians) {

    });
};

