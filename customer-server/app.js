var express = require("express");
var logfmt = require("logfmt");
var http = require('http');
var path = require('path');
var bodyParser = require('body-parser');
var favicon = require('serve-favicon');
var errorhandler = require('errorhandler');
var logger = require('morgan');
var methodOverride = require('method-override');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var ApplicationSettings = require('./config/ApplicationSettings');
var args = process.argv;
if (!args[2]) {
    ApplicationSettings.ENV = 'development';
} else {
    ApplicationSettings.ENV = args[2];
}

console.log("Using envoirnment " + ApplicationSettings.ENV);


if (!ApplicationSettings.hasAllSettings()) {
    console.log("Couldn't load environment settings");
    sys.exit(-1);
}
var userlogin = require('./routes/userlogin');
var userProfile = require('./routes/userProfile');
var card = require('./routes/card');
var booking = require('./routes/booking');
var promotion = require('./routes/promotion');
mysqlLib = require('./routes/mysqlLib');


genVarSettings = require('./config/ApplicationSettings').generalVariableSettings();

profilePicBaseUrl = genVarSettings.profilePicBaseUrl;
servicePicBaseUrl = genVarSettings.servicePicBaseUrl;


var app = express();

// all environments
app.set('port', process.env.PORT || genVarSettings.port);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('json spaces', 1);
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(favicon(__dirname + '/views/download.ico'));
app.use(logger('dev'));
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
app.use(logfmt.requestLogger());


app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

// development only
if ('development' == app.get('env')) {
    app.use(errorhandler());
}

app.get('/test', function (req, res) {
    res.render('test');
});

app.use('/registerFromEmail', multipartMiddleware);
app.post('/registerFromEmail', userlogin.RegisterFromEmail);
app.post('/emailLogin', userlogin.EmailLogin);
app.post('/accessTokenLogin', userlogin.AccessTokenLogin);
app.post('/fbLogin', userlogin.FbLogin);
app.post('/forgotPassword', userlogin.ForgotPassword);
app.post('/userLogout', userlogin.UserLogout);
app.post('/check_token', userlogin.checkTokenFromLink);
app.post('/set_password', userlogin.setPasswordFromUserEmailAndPassword);
app.post('/sendAndroidPush', userlogin.SendAndroidPush);
app.post('/initialCall', userlogin.InitialCall);

app.post('/getProfileData', userProfile.GetProfileData);
app.use('/editProfileData', multipartMiddleware);
app.post('/editProfileData', userProfile.EditProfileData);
app.post('/feedbackByCustomer', userProfile.FeedbackByCustomer);
app.post('/changePassword', userProfile.ChangePasswordOfUser);


app.post('/partyBooking', booking.PartyBooking);
app.post('/normalBooking', booking.NormalBooking);
app.post('/myBookings', booking.MyBookings);
app.post('/bookingDetails', booking.BookingDetails);
app.post('/cancelBooking', booking.CancelBooking);
app.post('/getBookingStatus', booking.GetBookingStatus);
app.post('/giveRating', booking.GiveRating);
app.get('/scheduleBookingsCron', booking.ScheduleBookingsCron);


app.post('/addCreditCard', card.AddCreditCard);
app.post('/showCreditCard', card.ShowCreditCard);
app.post('/deleteCard', card.DeleteCreditCard);
app.post('/defaultCard', card.DefaultCard);

app.post('/applyPromotionCode', promotion.ApplyPromotionCode);


http.createServer(app).listen(app.get('port'), function () {
    console.log("Express server listening on port " + app.get('port'));
});